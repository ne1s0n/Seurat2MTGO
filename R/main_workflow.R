#This script serves as a track for the processing. As soon as the
#development progress it will become lighter and lighter, substituting 
#actual computation to function calls. At the end it will serve as 
#a reference for the vignette, and then it will be removed.

# CONFIGURATION -----------------------------------------------------------
Seurat2MTGO.folder = '/home/nelson/research/Seurat2MTGO'
outfolder = '/home/nelson/tmp/Seurat2MTGO'

# SETUP -------------------------------------------------------------------
library(Seurat)
source(file.path(Seurat2MTGO.folder, 'R', 'lib.R'))
dir.create(outfolder, showWarnings = FALSE, recursive = TRUE)
fn = get.filenames(outfolder)

# LOAD SEURAT RESULT OBJECTS ----------------------------------------------
load(file.path(Seurat2MTGO.folder, 'data', 'seurat_tutorial.rda'))

# FILTER SEURAT OUTPUT ----------------------------------------------------
#this should be done after Seurat clustering, so that to focus the remaining
#part of the analysis on a subset of cells (usually one of the found clusters).
#Moreover, analysis should focus of genes showing high degrees of variation
#in their expression.

#For developing purposes we simply subset to the first 100 (genes) x 50 (cells) matrix
all.genes = rownames(pbmc@data)
all.cells = pbmc@cell.names

good.genes = all.genes[1:100]
good.cells = all.cells[1:50]
pbmc@data = pbmc@data[good.genes, good.cells]

pbmc@data = pbmc@data[good.genes, good.cells]
#here we could use seurat subsetting function, like
#FilterCells(object = pbmc, cells.use = good.cells) #this command subsets cells

# PREPARE MTGO INPUT: Edges -----------------------------------------------
#TODO: one day we'll have a choice among several correlation functions, plus the option
#to use a user's defined one
#see https://gitlab.com/ne1s0n/Seurat2MTGO/issues/1#note_84266209
write.coexpressionMatrix(SeuratObject = pbmc, outfolder = outfolder, overwrite = TRUE)

# PREPARE MTGO INPUT: GO terms --------------------------------------------
warning('Creating fake go file, so that MTGO can run')

#all used terms
terms = read.table(fn$gene_list.filename, header = FALSE, stringsAsFactors = FALSE)$V1

#four GO terms per gene
GO = paste(sep='', 'GO:', 1:(length(terms)*4))

#putting together
GO.terms = data.frame(
  terms = c(terms, terms, terms, terms),
  GO = GO
)

#saving
write.table(GO.terms, file = fn$GO.filename, sep='\t', row.names = FALSE, col.names = FALSE, quote = FALSE)

# PREPARE MTGO INPUT: parameters file -------------------------------------
write.paramFile(outfolder, overwrite = TRUE)

# CALL MTGO ---------------------------------------------------------------
#executing MTGO
#remember to put any filename in shQuote to avoid problems with spaces
call.MTGO(outfolder)